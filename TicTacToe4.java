import java.util.Scanner;
  
/** TicTacToe4 gameboard size is 4x4
 * First player gives two coordinates.First row 0,1,2,3 and second column 0,1,2,3.
 * First begins x and after that 0 continues. After you have put your coordinates gameboard is shown.
 * Players put they marks after another and first 4 row wins. Tie is possible situation. Simple but addictive.
*/

public class TicTacToe4{
  
    public static void main(String[ ] args) {
          
        TicTacToe t = new TicTacToe();
        Scanner s = new Scanner(System.in);
        int x=0,y=0;
        do
        {
            System.out.println(t.player==t.X?"Player X turn":"Player 0 turn");
            System.out.println("Give first row and then column coordinates 0,1,2,3");
            x=s.nextInt();
            y=s.nextInt();
              
            t.putSign(x, y);
            System.out.println(t.toString());
            System.out.println("===============");
            t.displayWinner();
              
        }while(t.isEmpty);
    }
}
  
class TicTacToe
{
    //Players + ja -
    public static final int X = 1, O = -1;
    public static final int EMPTY = 0;
      
    public int player = X;
    //Gameboard size 4x4
    private int[][] board = new int[4][4];
    public boolean isEmpty = false;
      
    /** Puts X or O in place i,j. */
    public void putSign(int x, int y)
    {
        // If x or y over tai under 0 , 3 
        if(x<0 || x>3 || y<0 || y>3)
        {
            System.out.println("Over the edge");
            return;
        }
        // If place is already used 
        if(board[x][y] != EMPTY)
        {
            System.out.println("Place is used");
            return;
        }
        board[x][y] = player;   
        player = -player;       // Changes player
    }
      
    /** Checks does player have 4 row to win */
    public boolean isWin(int player)
    {
        return ((board[0][0] + board[0][1] + board[0][2] + board[0][3] == player*4) ||
                (board[1][0] + board[1][1] + board[1][2] + board[1][3] == player*4) ||
                (board[2][0] + board[2][1] + board[2][2] + board[2][3] == player*4) ||
                (board[3][0] + board[3][1] + board[3][2] + board[3][3] == player*4) ||
                (board[0][0] + board[1][0] + board[2][0] + board[3][0] == player*4) ||
                (board[0][1] + board[1][1] + board[2][1] + board[3][1] == player*4) ||
                (board[0][2] + board[1][2] + board[2][2] + board[3][2] == player*4) ||
                (board[0][3] + board[1][3] + board[2][3] + board[3][3] == player*4) ||
                (board[0][0] + board[1][1] + board[2][2] + board[3][3] == player*4) ||
                (board[3][0] + board[1][2] + board[2][1] + board[0][3] == player*4));
    }
      
    /**Announces who wins or looses or flips a coin*/
    public void displayWinner()
    {
        if(isWin(X))
        {
            System.out.println("\n X Won");
            isEmpty=false;
        }
        else if(isWin(O))
        {
            System.out.println("\n O Won");
            isEmpty=false;
        }
        else
        {
            if(!isEmpty)
            {
                System.out.println("Tie !");
            }
              
        }
    }
      
    public String toString()
    {
        StringBuilder s = new StringBuilder();
        isEmpty = false;
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                switch(board[i][j])
                {
                case X:
                    s.append(" X ");
                    break;
                case O:
                    s.append(" O ");
                    break;
                case EMPTY:
                    s.append("   ");
                    isEmpty=true;
                    break;
                }
                if(j<3)
                {
                    s.append("|");
                }
                  
            }
            if(i<3)
            {
                s.append("\n===============\n");
            }
        }
        return s.toString();
    }
}